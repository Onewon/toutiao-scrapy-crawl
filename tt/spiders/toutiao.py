# -*- coding: utf-8 -*-
import scrapy,re,json,pymongo
from ..items import TtItem
from lxml import etree

index_count=0
type_list = ["finance",#0
            "society",#1
            "tech",#2
            "entertainment",#3
            "fashion",#4
            "game",#5
            "sports",#6
            "military",#7
            "world"#8
            ]

def get_news_url(group_number):
    template = "http://a3.pstatp.com/article/content/21/1/"+group_number+"/"+group_number+"/1/0/?iid=36660823810&device_id=48522746720&ac=wifi&channel=360&aid=13&app_name=news_article&version_code=677&version_name=6.7.7&device_platform=android"
    return template
def generate_url(article_ty,count):
    template = "http://lf.snssdk.com/api/news/feed/v82/?list_count="+str(count)+"&category=news_"+article_ty+"&concern_id=6215497900357585410&refer=1&refresh_reason=6&session_refresh_idx=2&count=100&min_behot_time=1530510912&last_refresh_sub_entrance_interval=25&loc_mode=5&loc_time=1530453210&latitude=22.480256666666666&longitude=113.90281666666667&lac=9518&cid=4171&plugin_enable=3&strict=1&iid=36660823810&device_id=48522746720&ac=wifi&channel=360&aid=13&app_name=news_article&version_code=677&version_name=6.7.7&device_platform=android"    
    return template

art_type = type_list[6]

class ToutiaoSpider(scrapy.Spider):
    name = 'toutiao'
    allowed_domains = ['a3.pstatp.com','lf.snssdk.com']
    
    def start_requests(self):
        yield scrapy.Request(generate_url(art_type,0), self.index_page)
    
    def index_page(self,response):
        global index_count
        index_count +=10
        if (index_count>=100):
            index_count = 0
        html = json.loads(response.text)
        nest_list = html['data']
        length = html['total_number']

        for i in range(length):
            content = nest_list[i]
            inside_nest = content.get("content")
            inside_nest = inside_nest.encode("utf-8")
            data = json.loads(inside_nest)
            the_aim = data.get("article_url")
            if the_aim:
                if "http://toutiao.com/group/" in the_aim:
                    group_number = re.findall(r"http://toutiao.com/group/(.*)/$",the_aim)
                    href = get_news_url(group_number[0])
                    yield scrapy.Request(href, callback=self.parse_detail)
                elif "https://m.toutiaocdn.com/group/" in the_aim:
                    group_number = re.findall(r"https://m.toutiaocdn.com/group/(.*)/",the_aim)
                    href = get_news_url(group_number[0])
                    yield scrapy.Request(href, callback=self.parse_detail)
                elif "https://m.toutiaocdn.cn/group/" in the_aim:
                    group_number = re.findall(r"https://m.toutiaocdn.cn/group/(.*)/",the_aim)
                    href = get_news_url(group_number[0])
                    yield scrapy.Request(href, callback=self.parse_detail)
            else:
                pass
        next_page = generate_url(art_type,index_count)
        yield scrapy.Request(next_page,dont_filter=True,callback=self.index_page)

    def parse_detail(self,response):
        article=title=""
        html = response.text
        html = html.encode("utf-8")
        url = response.url
        res = json.loads(response.text)
        meta = res['data']
        html = meta.get("content")
        selector = etree.HTML(html)
        ttl = selector.xpath("/html/body/header/div/text()")
        artlist = selector.xpath("/html/body/article/descendant::text()")
        for i in artlist:
            article +=i+"\n"
        article = article
        for tit in ttl:
            title += tit
        title = title
        
        item = TtItem()
        item['url'] = url
        item['title'] = title
        item['article'] = article
        yield item
        
        