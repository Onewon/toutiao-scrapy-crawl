#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# Created on 2018-06-20 09:22:09
# Project: toutiao

from pyspider.libs.base_handler import *
import re,json,pymongo
from pyspider.libs.response import Response
from lxml import etree

index_count=30

def generate_url(article_ty,count):
    template = "http://lf.snssdk.com/api/news/feed/v82/?list_count="+str(count)+"&category=news_"+article_ty+"&concern_id=6215497900357585410&refer=1&refresh_reason=6&session_refresh_idx=2&count=50&min_behot_time=1530510912&last_refresh_sub_entrance_interval=25&loc_mode=5&loc_time=1530453210&latitude=22.480256666666666&longitude=113.90281666666667&lac=9518&cid=4171&plugin_enable=3&strict=1&iid=36660823810&device_id=48522746720&ac=wifi&channel=360&aid=13&app_name=news_article&version_code=677&version_name=6.7.7&device_platform=android"    
    return template

def get_news_url(group_number):
    template = "http://a3.pstatp.com/article/content/21/1/"+group_number+"/"+group_number+"/1/0/?iid=36660823810&device_id=48522746720&ac=wifi&channel=360&aid=13&app_name=news_article&version_code=677&version_name=6.7.7&device_platform=android"
    return template
    
class Handler(BaseHandler):
    get_list_hdr = {
        "Accept-Encoding": "gzip",
        "X-SS-REQ-TICKET": "1530510938647",
        "User-Agent": "Dalvik/1.6.0 (Linux; U; Android 4.4.4; LON-AL00 Build/KTU84P) NewsArticle/6.7.7 okhttp/3.10.0.1",
        "Cookie": "odin_tt=ae0e3f1d09e6555ff38d0252f435d723f4c07aa181b2f3c79744a7256832b442b14db35b338b490a7bbaad630ea9edb5; alert_coverage=35; install_id=36660823810; ttreq=1$a27fd614d32f5c55cc624649190797a7cda8c1e3; qh[360]=1",
        "Host": "lf.snssdk.com",
        "Connection": "Keep-Alive"
    }
    crawl_config = {
         "itag": "v3.8"
    }
    client = pymongo.MongoClient('localhost')
    db = client['article']

    @every(minutes=24 * 60)
    def on_start(self):
        self.crawl(generate_url("tech",20),callback=self.index_page)

    @config(age=10 * 24 * 60 * 60)
    def index_page(self, response):
        global index_count
        index_count +=10
        resp = json.loads(response.text)
        nest_list = resp['data']
        length = resp ['total_number']
        for i in range(length):
            content = nest_list[i]
            inside_nest = content.get("content")
            inside_nest = inside_nest.encode("utf-8")
            data = json.loads(inside_nest)
            the_aim = data.get("article_url")
            if "http://toutiao.com/group/" in the_aim:
                group_number = re.findall(r"http://toutiao.com/group/(.*)/$",the_aim)
                #print (group_number)
                href = get_news_url(group_number[0])
                #print (href)
                self.crawl(href, callback=self.detail_page)
            else:
                pass
        next = generate_url("tech",index_count) 
        self.crawl(next,callback=self.index_page,headers = self.get_list_hdr)

    @config(priority=2)
    def detail_page(self, response):
        article = response.doc('article > div > p').text().encode("utf-8")
        meta = response.json['data']
        html = meta.get("content")
        selector = etree.HTML(html)
        title = selector.xpath("/html/body/header/div/text()")
        try:
            title = title[0]
        except Exception as e:
            pass
        else:
            pass
        
        return {
        "title": title,
        "article": article.decode("unicode-escape")
        }
    
    def on_result(self,result):
        if result:
            #print (result)
            self.save_to_mongo(result)

    def save_to_mongo(self,result):
        if self.db['toutiao_tech'].insert(result):
            print ('saved to mongo',result)